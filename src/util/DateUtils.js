export const DateUtils = {
    data() {
        return {
            weekDays: ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"],
            months: [
                "Gennaio",
                "Febbraio",
                "Marzo",
                "Aprile",
                "Maggio",
                "Giugno",
                "Luglio",
                "Agosto",
                "Settembre",
                "Ottobre",
                "Novembre",
                "Dicembre"
            ]
        }
    },
    methods: {
        getTitleDate(dateString) {
            if (dateString != null && dateString.length > 0) {
                let dateArray = dateString.split("-");
                return "dal " + dateArray[2] + " " + this.months[dateArray[1] - 1];
            }
            return null;
        }
    }
}
