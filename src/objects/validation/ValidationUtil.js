

export const ValidationUtils = {

    execLocalExpression(body, value,mapParam,formFieldValue) {
        if (body != undefined && body.type == "EXPRESSION" && value != "" && value != undefined) {

            // let expression = this.validateExpression(body)
            let matcherAfter = /^([0-9a-zA-Z\/\[\]\,\w\'\.\é\è\ò\à\ù\ì\(\)\:])+/;
            //^(?![==|>=|>|!=|in])([0-9a-zA-Z\/\[\]\,\w\'\.\é\è\ò\à\ù\ì])+/;
            let matcherOperator = /(==|>=|>|!=|in)/;
            let matcherBefore = /(([0-9a-zA-Z\/\[\]\,\w\'\.\é\è\ò\à\ù\ì\(\)\:]+)$)/
            //((==|>=|>|!=|in)*([0-9\/\[\]\,\w\'\.\é\è\ò\à\ù\ì]+)$)/
            let operator = matcherOperator.exec(body.expr);
            let before = matcherBefore.exec(body.expr);
            let after = matcherAfter.exec(body.expr);
            let beforeValid = this.validateExpression(before[0],value,mapParam,formFieldValue);
            let afterValid = this.validateExpression(after[0],value,mapParam,formFieldValue);
            switch (operator[0].toUpperCase()) {
                case "==":
                    return beforeValid == afterValid;
                case ">":
                    return beforeValid > afterValid;
                case ">=":
                    return beforeValid >= afterValid;
                case "!=":
                    return before != after;
                case "IN":
                    if (Array.isArray(after))
                        return after.find(a => a == beafore).length >= 1

                    return after.toUpperCase().includes(before.toUpperCase());
                    break;

            }

        }
        return true;

    },
    validateExpression(body,value,mapParam,formFieldValue) {
        //sizeOf(:self),:self,curdate
        let expression = body
            .replace(/:self/g, value)
            .replace(/((\w*curdate\w*)[2-9a-zA-z\+]+)/, function () {
                let n = body.split("+");
                let data = new Date();
                if (n.length > 0) {
                    let ydm = n[1].match(/[myd]/)
                    let number = n[1].match(/[0-9]/)
                    let day = ydm[0] == "d" ? data.getDate().getDate() + number[0] : data.getDate().getDate();
                    let year = ydm[0] == "y" ? data.getDate().getFullYear() + number[0] : data.getDate().getFullYear();
                    let month = ydm[0] == "m" ? data.getDate().getMonth() + number[0] : data.getDate().getMonth();
                    data = day + "/" + month + "/" + year;
                }
                return data;
            })
            .replace(/((\w*param\w*)[\(2-9\)a-zA-z\w\'\.\é\è\ò\à\ù\ì]+)/, function () {
                let val = body.replace("param(", "").replace(")", "");
                if(mapParam != undefined){
                    return mapParam[val];
                }
            })
            .replace(/((\w*sizeOf\w*)[\(2-9\)a-zA-z\w\'\.\é\è\ò\à\ù\ì]+)/, function () {
                let pr = "";
                if((new RegExp(/:self/g)).test(body)){
                 pr = body.replace(/:self/g, value);
                }else{
                   let id = body.match(/([2-9]+)/g);
                   if (formFieldValue != undefined && formFieldValue.length!=0){
                      let element = formFieldValue.find(a=> a.id == id);
                      pr = body.replace(/(:[2-9]+)/g,element != undefined ? element.value : "");
                   }else{
                       return 0;
                   }
                }
                let val = pr.replace("sizeOf(", "").replace(")", "");
                return val.length;
            }).replace(/(:[2-9]+)/g, function () {
                let id = body.match(/([2-9]+)/g);
                if (formFieldValue != undefined && formFieldValue.length!=0) {
                    let element = formFieldValue.find(a => a.id == id);
                    return  body.replace(/(:[2-9]+)/g, element != undefined ? element.value : "");
                }
                    return 0;

            })
        return expression;
    },


}

