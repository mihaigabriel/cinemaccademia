import VueI18n from "vue-i18n";
import Vue from "vue";
import {Messages} from "./Messages";

Vue.use(VueI18n);

export const I18n = new VueI18n({
	messages: Messages,
	silentFallbackWarn: true,
	silentTranslationWarn: true,
});
