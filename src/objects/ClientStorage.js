import {ClientStorageUtil} from "../_netStudio/utils/ClientStorageUtil";

const STORAGE_KEY = {
    LOGGED_USER_ID: "loggedUserId",
    USER: "user",
    AUTH_DELEGATE: "authDelegate",
    USER_LOCALE: "userLocale",
    DEFAULT_LOCALE: "defaultLocale",
    FALLBACK_LOCALE: "fallbackLocale",
}

export const ClientStorage = {
    getLoggedUserId() {
        return parseId(ClientStorageUtil.getValue(STORAGE_KEY.LOGGED_USER_ID, true));
    },
    setLoggedUserId(payload) {
        ClientStorageUtil.setValue(STORAGE_KEY.LOGGED_USER_ID, payload, true);
    },
    removeLoggedUserId() {
        ClientStorageUtil.removeKey(STORAGE_KEY.LOGGED_USER_ID, true);
    },
//--------------------------------------------------------------------------------
    getUserId() {
        return parseId(ClientStorageUtil.getValue(STORAGE_KEY.USER, true));
    },
    setUserId(payload) {
        ClientStorageUtil.setValue(STORAGE_KEY.USER, payload, true);
    },
    deleteUserId() {
        ClientStorageUtil.removeKey(STORAGE_KEY.USER, true);
    },
//--------------------------------------------------------------------------------
    getAuthDelegate() {
        return ClientStorageUtil.getValue(STORAGE_KEY.AUTH_DELEGATE, true);
    },
    setAuthDelegate(payload) {
        ClientStorageUtil.setValue(STORAGE_KEY.AUTH_DELEGATE, payload, true);
    },
    deleteAuthDelegate() {
        ClientStorageUtil.removeKey(STORAGE_KEY.AUTH_DELEGATE, true);
    },
//--------------------------------------------------------------------------------
    getUserLocale() {
        return ClientStorageUtil.getValue(STORAGE_KEY.USER_LOCALE, true);
    },
    setUserLocale(payload) {
        ClientStorageUtil.setValue(STORAGE_KEY.USER_LOCALE, payload, true);
    },
    removeUserLocale() {
        ClientStorageUtil.removeKey(STORAGE_KEY.USER_LOCALE, true);
    },
//--------------------------------------------------------------------------------
    getDefaultLocale() {
       return ClientStorageUtil.getValue(STORAGE_KEY.DEFAULT_LOCALE, true);
    },
    setDefaultLocale(payload) {
        ClientStorageUtil.setValue(STORAGE_KEY.DEFAULT_LOCALE, payload, true);
    },
    deleteDefaultLocale() {
        ClientStorageUtil.removeKey(STORAGE_KEY.DEFAULT_LOCALE, true);
    },
//--------------------------------------------------------------------------------
    getFallbackLocale() {
        return ClientStorageUtil.getValue(STORAGE_KEY.FALLBACK_LOCALE, true);
    },
    setFallbackLocale(payload) {
        ClientStorageUtil.setValue(STORAGE_KEY.FALLBACK_LOCALE, payload, true);
    },
    deleteFallbackLocale() {
        ClientStorageUtil.removeKey(STORAGE_KEY.FALLBACK_LOCALE, true);
    }
}

let parseId = function(value) {
    if (value)
        return parseInt(value);
    else
        return undefined;
}