module.exports = {
    extends: [
        'plugin:vue/base',
        'plugin:vue/recommended',
        'plugin:vue/essential',
        'plugin:vue/strongly-recommended'
    ],
    rules: {
        "no-unused-vars": "off",
        "max-attributes-per-line": "off",
        "html-indent": "off",
        "html-self-closing": "off",
        "singleline-html-element-content-newline": "off",
        "attributes-order": "off",
        "attribute-hyphenation": "off",
        "order-in-components": "off"
    }
}